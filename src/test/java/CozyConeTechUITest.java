import com.dxdg.pageobjects.AboutUs;
import com.dxdg.pageobjects.ContactUs;
import com.dxdg.pageobjects.HomePage;
import com.dxdg.pageobjects.ShippingReturns;
import com.dxdg.util.WebUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by Steven on 2015/5/24.
 */
public class CozyConeTechUITest {

    WebDriver driver;

    @Before
    public void setDriver(){
        String browserName = System.getenv("browser");
        if(browserName!=null && browserName.equalsIgnoreCase("Chrome")){
            driver = new ChromeDriver();
        } else {
            driver = new FirefoxDriver();
        }
    }

    @Test
    public void browserAllPages(){
        // 1. Home
        HomePage homePage = WebUtil.gHomePage(driver);
        // 2. Shipping & Returns
        ShippingReturns shippingReturns = homePage.clickShippingReturnsLinkage(driver);
        // 3. About Us
        AboutUs aboutUs = shippingReturns.clickAboutUsLinkage(driver);
        // 4. Contact Us
        ContactUs contactUs = aboutUs.clickContactUsLinkage(driver);
        // 5. Send Contact Us Message
        // 5.1 Prepare Data
        final String contactName = "Selenium";
        final String contactEmail = "Selenium@gmail.com";
        final String contactPhone = "0987654321";
        final String contactMessage = "Hi This is an automation test";
        // 5.2 Fill Contact Name
        contactUs.fillContactName(driver, By.cssSelector("input[data-name='name']"), contactName);
        // 5.3 Fill Contact Email
        contactUs.fillContactEmail(driver, By.cssSelector("input[data-name='email_address']"), contactEmail);
        // 5.4 Fill Contact Phone
        contactUs.fillContactPhone(driver, By.cssSelector("input[data-name='phone_number']"), contactPhone);
        // 5.5 Fill Contact Message
        contactUs.fillContactMessage(driver, By.cssSelector("textarea[data-name='message']"), contactMessage);
        // 5.6 Click Send Message Button
        contactUs.clickSendMessageButton(driver, By.cssSelector("a[data-name='send_message']"));
        // 5.7 Verify if message sent

        Assert.assertTrue("Success Message should be exist", contactUs.isSuccessMessageExist(driver, By.id("text-success")));
    }

    @After
    public void tearDown(){
        driver.quit();
    }

}
