package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class ShippingReturns {

    public AboutUs clickAboutUsLinkage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("About Us"));

        WebUtil.click(driver, By.linkText("About Us"));

        return PageFactory.initElements(driver, AboutUs.class);
    }
}
