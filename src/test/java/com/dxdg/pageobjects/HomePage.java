package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class HomePage {

    public ShippingReturns clickShippingReturnsLinkage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("Shipping & Returns"));

        WebUtil.click(driver, By.linkText("Shipping & Returns"));

        return PageFactory.initElements(driver, ShippingReturns.class);
    }

}
