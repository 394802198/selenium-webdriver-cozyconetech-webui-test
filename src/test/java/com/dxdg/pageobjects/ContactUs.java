package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Steven on 2015/5/24.
 */
public class ContactUs {

    public void fillContactName(WebDriver driver, By by, String contactName) {
        WebUtil.waitForElementVisible(driver, by);

        WebUtil.clearAndSendKeys(driver, by, contactName);
    }

    public void fillContactEmail(WebDriver driver, By by, String contactEmail) {
        WebUtil.clearAndSendKeys(driver, by, contactEmail);
    }

    public void fillContactPhone(WebDriver driver, By by, String contactPhone) {
        WebUtil.clearAndSendKeys(driver, by, contactPhone);
    }

    public void fillContactMessage(WebDriver driver, By by, String contactMessage) {
        WebUtil.clearAndSendKeys(driver, by, contactMessage);
    }

    public void clickSendMessageButton(WebDriver driver, By by) {
        WebUtil.click(driver, by);
    }

    public boolean isSuccessMessageExist(WebDriver driver, By by) {
        WebUtil.waitForElementVisible(driver, by);

        return WebUtil.isElementExist(driver, by);
    }
}
